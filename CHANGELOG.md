# Changelog

This file describes the evolution of the *CD Cover* LaTeX template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.1.1 (November 2019)

+ Fix for the incorrect position of the full image. 

## Version 0.1.0 (November 2019)

+ Title can be set from YAML.
+ Subtitle can be set from YAML.
+ Cover image can be set from YAML.
+ Track list can be set from YAML.
+ Colors can be customized from YAML.

