---
# Title.
title: Example of CD Cover
subtitle: Made with Pandoc

# Image.
full-image: cd.png
image-width: 11cm

# Tracks.
track:
    - artist: Great Singer
      title: Wonderful Song
    - artist: Awesome Band
      title: Incredible Melody
    - artist: Amazing DJ
      title: Glorious Mix
    - artist: Great Singer
      title: Wonderful Song
    - artist: Awesome Band
      title: Incredible Melody
    - artist: Amazing DJ
      title: Glorious Mix
    - artist: Great Singer
      title: Wonderful Song
    - artist: Awesome Band
      title: Incredible Melody
    - artist: Amazing DJ
      title: Glorious Mix
    - artist: Great Singer
      title: Wonderful Song
    - artist: Awesome Band
      title: Incredible Melody
    - artist: Amazing DJ
      title: Glorious Mix
    - artist: Great Singer
      title: Wonderful Song
    - artist: Awesome Band
      title: Incredible Melody
    - artist: Amazing DJ
      title: Glorious Mix
    - artist: Great Singer
      title: Wonderful Song
    - artist: Awesome Band
      title: Incredible Melody
    - artist: Amazing DJ
      title: Glorious Mix

# Columns.
columns: 2

# Color Settings.
title-color: teal
subtitle-color: olive
---
