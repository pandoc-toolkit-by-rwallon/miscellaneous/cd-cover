---
# Title.
title: Example of CD Cover
subtitle: Made with Pandoc

# Image.
image: cd.png

# Tracks.
track:
    - artist: Great Singer
      title: Wonderful Song
      length: 3'42
    - artist: Awesome Band
      title: Incredible Melody
      length: 4'51
    - artist: Amazing DJ
      title: Glorious Mix
      length: 3'24
    - artist: Great Singer
      title: Wonderful Song
      length: 3'42
    - artist: Awesome Band
      title: Incredible Melody
      length: 4'51
    - artist: Amazing DJ
      title: Glorious Mix
      length: 3'24
    - artist: Great Singer
      title: Wonderful Song
      length: 3'42
    - artist: Awesome Band
      title: Incredible Melody
      length: 4'51
    - artist: Amazing DJ
      title: Glorious Mix
      length: 3'24
    - artist: Great Singer
      title: Wonderful Song
      length: 3'42
    - artist: Awesome Band
      title: Incredible Melody
      length: 4'51
    - artist: Amazing DJ
      title: Glorious Mix
      length: 3'24
    - artist: Great Singer
      title: Wonderful Song
      length: 3'42
    - artist: Awesome Band
      title: Incredible Melody
      length: 4'51
    - artist: Amazing DJ
      title: Glorious Mix
      length: 3'24
    - artist: Great Singer
      title: Wonderful Song
      length: 3'42
    - artist: Awesome Band
      title: Incredible Melody
      length: 4'51
    - artist: Amazing DJ
      title: Glorious Mix
      length: 3'24

# Color Definitions.
color:
    - name: myblue
      type: RGB
      value: 107,174,214
    - name: mygreen
      type: RGB
      value: 102,255,204
    - name: myblack
      type: RGB
      value: 24,24,24

# Color Settings.
title-color: myblue
subtitle-color: mygreen
track-color: white
background-color: myblack
---
