# Use Cases of the *CD Cover* Template

This directory contains examples illustrating different use cases of the
*CD Cover* Pandoc template.

For each of the examples, you may either read its metadata file (to see how we
configured it) or download the final PDF.

## Single Column

This CD cover displays the track list on a single column, and defines its
own colors for the background, the title, the subtitle and the track list.

The tracks in the list specify their artist, their title and their length.

*Metadata file available [here](single-column.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/single-column.pdf?job=make-examples).*

## Two Columns

This CD cover uses less colors, but displays the track list on two columns.
Also, the image on the cover is used as a full image.

The tracks in the list do not have their length specified.

*Metadata file available [here](two-columns.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/two-columns.pdf?job=make-examples).*
