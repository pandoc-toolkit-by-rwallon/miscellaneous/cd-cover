---
# Title.
title:
subtitle:

# Image.
image:
full-image:
image-width:

# Tracks.
track:
    - artist:
      title:
      length:

# Columns.
columns:

# Color Definitions.
color:
    - name:
      type:
      value:

# Color Settings.
title-color:
subtitle-color:
track-color:
background-color:
---
