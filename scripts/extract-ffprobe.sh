#!/usr/bin/env bash
#
# Extracts with 'ffprobe' the metadata from the audio files contained in
# the directory given as argument, and saves them in a Markdown file named
# with the name of this directory in the format expected by the template.
#
# Argument:
#   $1 - The input directory containing the audio files to consider.


#############
# FUNCTIONS #
#############


##############################################################################
# Extracts the metadata of the audio files contained in the given directory
# using 'ffbrobe' to the standard output.
#
# Argument:
#   $1 - The input directory containing the audio files to consider.
#
# Returns:
#   None
##############################################################################
function extractMetadata {
    local file
    for file in $(ls "$1")
    do
        ffprobe "$1/$file" 2>&1
    done
}


################
# MAIN PROGRAM #
################


# IFS is set to CR to allow spaces in file names.
export IFS="
"

# Verifying the command line.
test $# -ne 1 && echo "Usage: $0 <input-directory>" && exit 1
test ! -d "$1" && echo "Usage: $0 <input-directory>" && exit 2

# Extracting the metadata.
extractMetadata "$1" | "$(dirname "$0")/extract-ffprobe.pl" > "$(basename "$1").md"
