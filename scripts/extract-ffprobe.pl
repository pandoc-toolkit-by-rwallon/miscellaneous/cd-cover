#!/usr/bin/env perl
#
# Reads from the standard input the output of 'ffprobe' to extract the metadata
# required by the template, and writes to the standard output the corresponding
# YAML configuration.

use strict;
use warnings;

########################
# Track Initialization #
########################

my @tracks = ();
my $current_track = undef;

######################
# Reading the Tracks #
######################

while (<>) {
    if (/Input/) {
        $current_track = {};
        push(@tracks, $current_track);

    } elsif (/title\s*:\s*(.*)/) {
        $current_track->{title} = "$1";

    } elsif (/artist\s*:\s*(.*)/) {
        $current_track->{artist} = "$1";

    } elsif (/Duration\s*:\s*00:(\d\d):(\d\d)/) {
        $current_track->{length} = "$1'$2";
    }
}

#######################
# Printing the Tracks #
#######################

print("---\n");
print("tracks:\n");
foreach my $track (@tracks) {
    printf("    - title:  %s\n", $track->{title});
    printf("      artist: %s\n", $track->{artist}) if (exists($track->{artist}));
    printf("      length: %s\n", $track->{length});
}
print("---\n");
