# Scripts for Writing Track Lists

This directory contains some scripts allowing to automatically create your
track lists from audio files.

## Requirements

To use these scripts, you need to install [`ffmpeg`](https://ffmpeg.org/) on
your computer, since they use its [`ffprobe`](https://ffmpeg.org/ffprobe.html)
tool.

Also, the audio files to consider must have their metadata set appropriately.

## Creating the Track List

Suppose the directory containing your audio files is named `my-music`.
To create the corresponding track list, simply run the following command.

```bash
/path/to/scripts/extract-ffprobe.sh my-music
```

This will create a file `my-music.md` containing all the informations available
about the files contained in the directory, as expected by the template.

## License

The scripts are distributed under the *Unlicense* license.

> This is free and unencumbered software released into the public domain.
>
> Anyone is free to copy, modify, publish, use, compile, sell, or distribute
> this software, either in source code form or as a compiled binary, for any
> purpose, commercial or non-commercial, and by any means.
>
> In jurisdictions that recognize copyright laws, the author or authors of this
> software dedicate any and all copyright interest in the software to the
> public domain.
> We make this dedication for the benefit of the public at large and to the
> detriment of our heirs and successors.
> We intend this dedication to be an overt act of relinquishment in perpetuity
> of all present and future rights to this software under copyright law.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
> IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.
>
> For more information, please refer to <http://unlicense.org/>
