# Pandoc Template for a *CD Cover*

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/cd-cover/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/miscellaneous/cd-cover/commits/master)

## Description

This project provides a template allowing an easy creation of CD covers
with [Pandoc](https://pandoc.org).

This template is distributed under a Creative Commons Attribution 4.0
International License.

[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)

## Requirements

To build your CD cover using this template, [Pandoc](https://pandoc.org)
at least 2.0 have to be installed on your computer.

You also need to have the class
[`cd-cover`](https://www.ctan.org/tex-archive/macros/latex/contrib/cd-cover)
installed in your [LaTeX](https://www.latex-project.org) distribution.

## Creating your CD Cover

To create your cover, you need to setup its YAML configuration.
For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.
Read below for details on how to configure your cover, and see our various
[examples](examples).

### Title

You may specify the `title` and `subtitle` of the CD in the corresponding
fields.

Note that the subtitle is optional, and may also be used to specify the main
artist, if any.

### Image

You may put an (optional) image on your cover by specifying its path in
`image`.
You may customize its width thanks to `image-width`.

Also, if `full-image` is specified instead of `image`, then this image is the
only thing appearing on the main cover.
Note that `image-width` is still taken into account in this case.

### Tracks

Tracks are listed within the `track` list, and are defined through three
fields: their `artist`, `title` and `length`.
Only the title is required.

To help you writing the track list, we provide some [scripts](scripts) based
on [`ffprobe`](https://ffmpeg.org/ffprobe.html) allowing to automatically
create this list from your audio files.

### Columns

By default, the tracks of the CD are put in a single column.
You may specify that you want the track list to use more than one column by
setting `columns` to the expected number of columns.

### Colors

#### Color Definitions

You may define your own colors through the variable `color`, which must be
set to a list of objects defining three fields:

+ `name`: the name of the color you are defining.
+ `type`: the type of the color, among `gray`, `rgb`, `RGB`, `html`, `HTML`,
  etc.
+ `value`: the actual value of the color.

Note that each color is then automatically translated into LaTeX by using the
following command:

```latex
\definecolor{name}{type}{value}
```

#### Color Settings

Once you have defined your own colors, you may set the following properties
to customize the colors of your CD cover (of course, you may also use
predefined colors):

+ `title-color`: the color for the title of the CD.
+ `subtitle-color`: the color for the subtitle of the CD.
+ `track-color`: the color for the track list.
+ `background-color`: the color for the background of the cover.

By default, all text colors are set to `black`, and there is no background
color.

## Building your CD Cover

Suppose that you have written the metadata of the cover in a file `input.md`.
Then, to produce the CD cover in a file named `output.pdf`, execute the
following command:

```bash
pandoc --template cd-cover.pandoc input.md -o output.pdf
```
